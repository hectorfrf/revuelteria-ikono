(function(){
  'use strict';

  var app_prueba_ikono = angular.module('app_prueba_ikono', ['ngResource']);

  app_prueba_ikono.config(function($resourceProvider) {
    $resourceProvider.defaults.actions = {  // Se renombra el metodo getAll por listar
      listar: {
        method: 'GET',
        isArray: true
      }
    };
  });

  app_prueba_ikono.controller('controladorPrincipal', function ($resource, $q) { // Se inyecta modulo para hacer peticiones y modulo para hacer promesas
    var $pCtr = this,
        _recurso =  $resource("https://demo4010019.mockable.io/products"),
        diferido = $q.defer();

    $pCtr.productos = [];
    function obtenerProductos(){
      _recurso.listar()
      .$promise // Se agrega un promesa para la peticion asincrona
      .then(function(respuesta){
        $pCtr.productos = respuesta;  // Se agrega todos los productos retornados a la variable que los grafica
        return diferido.resolve(respuesta); // Se indica que la promesa termina si salio bien
      }).catch(function(errores){
        return diferido.reject(errores);  // Se indica que la promesa termina si salio mal
      });
      return diferido.promise;
    }

    $pCtr.$onInit = function(){  // Solicitar productos al terminar de cargar los componentes de la pagina
      obtenerProductos();
    };

  	$pCtr.abrirModalProductos = function(producto){  // Como parametro llega el producto seleccionado para ver detalles
      $('#modalDeProductos').modal('show')
      $pCtr.productoDetalles = producto;  // Asignar el producto seleccionado a la variable que grafica los datos
  	};

  });

})();
